FROM eclipse-temurin:11-jre
LABEL authors="alex"

EXPOSE 8080:8080

RUN mkdir /app
COPY ./build/libs/*-all.jar /app/api.jar
ENTRYPOINT ["java","-jar","/app/api.jar"]