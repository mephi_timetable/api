package com.example.services

import com.example.services.timetableServices.TimetableServiceImpl
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.LocalDate
import org.junit.jupiter.api.Test
import org.koin.test.KoinTest
import org.koin.test.inject


class TimetableServiceImplTest : KoinTest {
    init {
//        startKoin {
//            modules(module {
//                singleOf(::GroupRepositoryImpl) { bind<GroupRepository>() }
//                singleOf(::Database) { createdAtStart() }
//                singleOf(::TimetableRepositoryImpl) { bind<TimetableRepository>() }
//                singleOf(::InnerGroupRepoImpl) { bind<InnerGroupRepo>() }
//                singleOf(::TimetableServiceImpl)
//            })
//        }
    }

    @Test
    fun `get timetable for certain group and date`() = runBlocking {
        val serviceImpl: TimetableServiceImpl by inject()
        val timetable = serviceImpl.getLessonsOfGroupAtDate("Б22-503",
            LocalDate.parse("2023-02-22"))
        timetable.forEach { println(it) }

    }
}