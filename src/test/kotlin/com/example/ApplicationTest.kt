package com.example

import com.example.configurations.Database
import com.example.controllers.groupsController
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.server.testing.*
import kotlin.test.*
import io.ktor.http.*
import com.example.trash.*
import database.repositories.groupRepo.GroupRepository
import io.ktor.server.routing.*
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.createdAtStart
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import org.koin.test.KoinTest
import database.repositories.groupRepo.GroupRepositoryImpl
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.koin.core.context.startKoin
import kotlin.time.measureTime

private val logger = KotlinLogging.logger {}

class ApplicationTest : KoinTest {
    val allGroups  = """
        ["Б22-101","Б22-102","Б22-103","Б22-104","Б22-105","Б22-161","Б22-163","Б22-201","Б22-202","Б22-203","Б22-204","Б22-205","Б22-206","Б22-211","Б22-212","Б22-215","Б22-221","Б22-222","Б22-261","Б22-265","Б22-301","Б22-302","Б22-311","Б22-361","Б22-362","Б22-401","Б22-403","Б22-413","Б22-464","Б22-501","Б22-502","Б22-503","Б22-504","Б22-505","Б22-511","Б22-513","Б22-514","Б22-515","Б22-524","Б22-525","Б22-534","Б22-544","Б22-554","Б22-564","Б22-565","Б22-601","Б22-602","Б22-603","Б22-604","Б22-605","Б22-612","Б22-701","Б22-702","Б22-703","Б22-712","Б22-761","Б22-762","Б22-763","Б22-801","Б22-802","Б22-803","Б22-901","Б22-902","Б22-В02","Б22-В71","Б21-101","Б21-102","Б21-103","Б21-104","Б21-107","Б21-161","Б21-162","Б21-201","Б21-202","Б21-203","Б21-204","Б21-205","Б21-206","Б21-211","Б21-212","Б21-215","Б21-221","Б21-261","Б21-265","Б21-301","Б21-302","Б21-311","Б21-362","Б21-401","Б21-403","Б21-405","Б21-413","Б21-465","Б21-502","Б21-503","Б21-504","Б21-505","Б21-513","Б21-514","Б21-515","Б21-524","Б21-525","Б21-534","Б21-563","Б21-564","Б21-565","Б21-601","Б21-602","Б21-603","Б21-604","Б21-611","Б21-701","Б21-702","Б21-703","Б21-763","Б21-801","Б21-802","Б21-803","Б21-901","Б21-902","Б21-В02","Б21-В03","Б21-В71","Б20-101","Б20-102","Б20-103","Б20-104","Б20-105","Б20-201","Б20-202","Б20-203","Б20-204","Б20-205","Б20-211","Б20-215","Б20-221","Б20-301","Б20-302","Б20-362","Б20-401","Б20-402","Б20-403","Б20-413","Б20-462","Б20-464","Б20-465","Б20-500","Б20-503","Б20-504","Б20-505","Б20-513","Б20-514","Б20-515","Б20-523","Б20-524","Б20-525","Б20-561","Б20-564","Б20-565","Б20-601","Б20-602","Б20-603","Б20-604","Б20-611","Б20-661","Б20-701","Б20-702","Б20-703","Б20-763","Б20-801","Б20-802","Б20-901","Б20-902","Б20-В02","Б20-В71","Б19-101","Б19-102","Б19-103","Б19-104","Б19-105","Б19-162","Б19-163","Б19-201","Б19-202","Б19-203","Б19-204","Б19-205","Б19-211","Б19-221","Б19-301","Б19-302","Б19-401","Б19-402","Б19-403","Б19-501","Б19-503","Б19-504","Б19-505","Б19-511","Б19-513","Б19-514","Б19-515","Б19-525","Б19-565","Б19-601","Б19-602","Б19-603","Б19-604","Б19-701","Б19-702","Б19-801","Б19-802","Б19-803","Б19-901","Б19-902","Б19-В01","Б19-В02","Б19-В03","Б18-В01","С22-101","С22-103","С22-161","С22-201","С22-391","С22-392","С22-393","С22-401","С22-402","С22-501","С22-602","С22-701","С22-702","С22-703","С22-711","С22-712","С22-761","С22-762","С21-101","С21-103","С21-113","С21-392","С21-393","С21-401","С21-402","С21-501","С21-602","С21-701","С21-702","С21-703","С21-711","С21-712","С21-762","С20-101","С20-103","С20-201","С20-391","С20-392","С20-393","С20-401","С20-402","С20-501","С20-602","С20-700","С20-701","С20-702","С20-703","С19-101","С19-103","С19-201","С19-401","С19-402","С19-501","С19-602","С19-701","С19-702","С19-711","С19-712","С19-761","С18-101","С18-103","С18-111","С18-163","С18-201","С18-202","С18-401","С18-402","С18-501","С18-602","С18-701","С18-702","С18-711","С18-712","С18-761","С17-В01","М22-100","М22-101","М22-102","М22-104","М22-105","М22-106","М22-108","М22-112","М22-113","М22-115","М22-117","М22-165","М22-190","М22-191","М22-198","М22-201","М22-202","М22-203","М22-205","М22-206","М22-207","М22-208","М22-209","М22-211","М22-213","М22-218","М22-281","М22-282","М22-283","М22-287","М22-288","М22-289","М22-301","М22-302","М22-303","М22-304","М22-305","М22-311","М22-312","М22-381","М22-383","М22-403","М22-405","М22-406","М22-413","М22-423","М22-481","М22-482","М22-501","М22-506","М22-507","М22-508","М22-512","М22-514","М22-517","М22-522","М22-524","М22-562","М22-564","М22-594","М22-601","М22-602","М22-612","М22-621","М22-684","М22-701","М22-703","М22-705","М22-711","М22-713","М22-721","М22-781","М22-801","М22-802","М22-902","М22-904","М22-905","М22-915","М22-964","М22-965","М22-966","М22-994","М22-В01","М22-В02","М22-В03","М22-Ш01","М22-Ш02","М22-Ш03","М22-Ш04","М21-100","М21-101","М21-102","М21-105","М21-106","М21-108","М21-113","М21-115","М21-117","М21-165","М21-166","М21-190","М21-191","М21-192","М21-197","М21-201","М21-202","М21-203","М21-204","М21-207","М21-208","М21-209","М21-210","М21-213","М21-216","М21-218","М21-283","М21-287","М21-288","М21-289","М21-301","М21-302","М21-303","М21-304","М21-305","М21-311","М21-312","М21-381","М21-383","М21-401","М21-403","М21-404","М21-405","М21-406","М21-413","М21-414","М21-423","М21-481","М21-482","М21-483","М21-484","М21-485","М21-502","М21-506","М21-507","М21-508","М21-512","М21-514","М21-517","М21-522","М21-524","М21-534","М21-564","М21-568","М21-582","М21-583","М21-584","М21-585","М21-587","М21-598","М21-601","М21-611","М21-612","М21-684","М21-701","М21-705","М21-711","М21-713","М21-721","М21-752","М21-765","М21-781","М21-782","М21-801","М21-902","М21-904","М21-915","М21-961","М21-962","М21-964","М21-965","М21-966","М21-967","М21-994","М21-В01","М21-В02","М21-В03","М21-Ш01","М21-Ш02","М21-Ш03","М21-Ш04","А22-101","А22-104","А22-105","А22-108","А22-121","А22-131","А22-201","А22-202","А22-204","А22-205","А22-206","А22-211","А22-212","А22-221","А22-301","А22-302","А22-303","А22-304","А22-313","А22-401","А22-402","А22-403","А22-413","А22-501","А22-502","А22-503","А22-601","А22-609","А22-623","А22-711","А22-712","А22-714","А22-901","А22-П01","А22-П02","А21-101","А21-104","А21-105","А21-106","А21-108","А21-111","А21-121","А21-131","А21-201","А21-202","А21-203","А21-204","А21-205","А21-206","А21-211","А21-212","А21-221","А21-222","А21-301","А21-302","А21-303","А21-304","А21-313","А21-401","А21-402","А21-403","А21-413","А21-423","А21-433","А21-501","А21-502","А21-503","А21-601","А21-609","А21-623","А21-702","А21-703","А21-711","А21-901","А20-101","А20-104","А20-105","А20-106","А20-108","А20-111","А20-121","А20-131","А20-201","А20-202","А20-204","А20-211","А20-212","А20-221","А20-222","А20-301","А20-302","А20-303","А20-304","А20-313","А20-401","А20-402","А20-403","А20-413","А20-501","А20-502","А20-503","А20-601","А20-609","А20-623","А20-702","А20-703","А20-711","А20-901","А19-101","А19-104","А19-105","А19-106","А19-108","А19-109","А19-111","А19-121","А19-131","А19-165","А19-201","А19-202","А19-211","А19-212","А19-213","А19-221","А19-223","А19-231","А19-302","А19-303","А19-304","А19-313","А19-402","А19-403","А19-413","А19-501","А19-502","А19-503","А19-504","А19-511","А19-601","А19-623","А19-703","А19-901"]
    """
    val allGroups1000times = (1..1000).map { allGroups + it.toString() }



    init {
        startKoin {
            modules(module {
                singleOf(::GroupRepositoryImpl) { bind<GroupRepository>() }
                singleOf(::Database) { createdAtStart() }
            })
        }
    }
    @Test
    fun testRoot() = testApplication {
        application {
            configureRouting()
        }
        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)
            assertEquals("Hello World!", bodyAsText())
        }
    }


    @Test
    fun testGetAllGroups() = testApplication {
//        mockkStatic(Route::groupsController)
//        val rout = slot<Route>()
//        every { capture(rout).groupsController() } coAnswers {
//            rout.captured {
//                get("/groups") {
//                    call.respond(allGroups)
//                }
//            }
////            callOriginal()
//        }

        application {
            routing {
                groupsController()
            }
        }
        measureTime {
            coroutineScope {
                repeat(100) {
                    launch {
                        client.get("/groups").apply {
                            assertEquals(HttpStatusCode.OK, status)
                        }
                    }
                }
            }
        }.also {
            logger.info(it.inWholeMilliseconds.toString())
        }

    }
}
