package com.example.configurations

import com.example.services.groupServices.GroupService
import com.example.services.groupServices.GroupServiceImpl
import com.example.services.timetableServices.TimetableService
import com.example.services.timetableServices.TimetableServiceImpl
import database.repositories.TimetableRepo
import database.repositories.TimetableRepoImpl
import database.repositories.groupRepo.GroupRepository
import database.repositories.groupRepo.GroupRepositoryImpl
import database.repositories.timetableRepo.TimetableRepository
import database.repositories.timetableRepo.TimetableRepositoryImpl
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.createdAtStart
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

val appModule = module {
    singleOf(::Database) { createdAtStart() }
    // repos
    singleOf(::TimetableRepoImpl) { bind<TimetableRepo>() }
    singleOf(::TimetableRepositoryImpl) { bind<TimetableRepository>() }
    singleOf(::GroupRepositoryImpl) { bind<GroupRepository>() }
    // services
    singleOf(::TimetableServiceImpl) { bind<TimetableService>()}
    singleOf(::GroupServiceImpl) { bind<GroupService>() }

}
