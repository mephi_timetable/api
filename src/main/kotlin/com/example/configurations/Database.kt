package com.example.configurations

import database.models.tables.GroupTable
import database.models.tables.TimetableTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

class Database {
    init {
//        Database.connect("jdbc:h2:tcp://localhost/~/test", driver = "org.h2.Driver",
//            user = "api", password = "api_password")
        Database.connect("jdbc:h2:tcp://h2-database:9092/h2-data",
            driver = "org.h2.Driver",
            user = "sa", password = "")
        transaction {
            SchemaUtils.create(GroupTable, TimetableTable)
        }
    }
}