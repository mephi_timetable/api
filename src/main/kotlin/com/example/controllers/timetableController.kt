package com.example.controllers

import com.example.services.timetableServices.TimetableServiceImpl
import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.datetime.toLocalDate
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.koin.ktor.ext.inject

private val logger = KotlinLogging.logger {}

fun Route.timetableController() {
    val timetableService: TimetableServiceImpl by inject()

    get("/groups/{group}/schedule/{day}") {
        val groupNum = call.parameters["group"] ?: throw RuntimeException("no group")
        val day = call.parameters["day"]?.toLocalDate()
            ?: throw IllegalArgumentException()
        val lessons = timetableService.getLessonsOfGroupAtDate(groupNum, day)
        call.respond(Json.encodeToString(lessons))
    }

    get("/groups/{group}/schedule") {
        val groupNum = call.parameters["group"] ?: throw RuntimeException("no group")
        val lessons = timetableService.getLessonsOfGroup(groupNum)
        call.respond(Json.encodeToString(lessons))
    }
}