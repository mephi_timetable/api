package com.example.controllers

import com.example.services.groupServices.GroupService
import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.koin.ktor.ext.inject

private val logger = KotlinLogging.logger {}

fun Route.groupsController() {
    val groupService: GroupService by inject()

    get("/groups") {
        val prefix = call.request.queryParameters["prefix"]
        val groups = if (prefix != null) {
            groupService.getGroupsNumsStartedWith(prefix)
        } else groupService.getAllGroupsNums()
        logger.debug(groups.toString())
        call.respond(Json.encodeToString(groups))
    }

    get("/groups/") {

    }
}