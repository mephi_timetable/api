package com.example.services.groupServices

interface GroupService {
    suspend fun getAllGroupsNums(): Set<String>
    suspend fun getGroupsNumsStartedWith(prefix: String): Set<String>
}