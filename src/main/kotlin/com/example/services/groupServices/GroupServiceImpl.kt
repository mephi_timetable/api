package com.example.services.groupServices

import database.repositories.TimetableRepo

class GroupServiceImpl(
    private val timetableRepo: TimetableRepo
) : GroupService {
    override suspend fun getAllGroupsNums() =
        timetableRepo.getAllGroups().map { it.groupNum }.toSet()

    override suspend fun getGroupsNumsStartedWith(prefix: String) =
        timetableRepo.getGroupsNumsStartedWith(prefix).map { it.groupNum }.toSet()


}