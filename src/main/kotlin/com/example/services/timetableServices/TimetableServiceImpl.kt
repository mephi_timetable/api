package com.example.services.timetableServices

import database.models.Lesson
import database.repositories.TimetableRepo
import kotlinx.datetime.LocalDate
import kotlinx.datetime.daysUntil

class TimetableServiceImpl(
    private val timetableRepo: TimetableRepo
) : TimetableService {
    private fun Lesson.isLessonAtDate(date: LocalDate) = run {
        until?.date?.let { untilDate ->
            if (lessonStart.date < date && date < untilDate) {
                if (repeat == "WEEKLY") {
                    date.daysUntil(lessonStart.date) % (7 * interval) == 0
                } else throw RuntimeException("Unknown repeat value")
            } else false
        } ?: (lessonStart.date == date)
    }

    override suspend fun getLessonsOfGroupAtDate(groupNum: String, date: LocalDate) =
        timetableRepo.getByGroupNum(groupNum)?.filter { it.isLessonAtDate(date) } ?: emptyList()

    override suspend fun getLessonsOfGroup(groupNum: String): List<Lesson> =
        timetableRepo.getByGroupNum(groupNum) ?: emptyList()


}