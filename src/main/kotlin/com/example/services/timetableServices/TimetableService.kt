package com.example.services.timetableServices

import database.models.Lesson
import kotlinx.datetime.LocalDate

interface TimetableService {
    suspend fun getLessonsOfGroupAtDate(groupNum: String, date: LocalDate): List<Lesson>
    suspend fun getLessonsOfGroup(groupNum: String): List<Lesson>

}