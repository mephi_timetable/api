package com.example

import com.example.configurations.appModule
import com.example.controllers.groupsController
import com.example.controllers.timetableController
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.routing.*
import org.koin.ktor.plugin.Koin
import org.koin.logger.slf4jLogger


fun main() {
    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    install(Koin) {
        slf4jLogger()
        modules(appModule)
    }
    routing {
        route("/api") {
            groupsController()
            timetableController()
        }
    }
//    configureSerialization()
//    configureDatabases()
//    configureRouting()
}
